//importing fetch function
import fetch from "node-fetch";

//function to fetch users and todos per user
function fetchUsersAndTodosPerUser() {
  const usersUrl = "https://jsonplaceholder.typicode.com/users";
  const todosUrl = "https://jsonplaceholder.typicode.com/todos";

  let resultObject = {};
  let finalObject = {};

  return fetch(usersUrl)
    .then((responseOfUsers) => {
      if (!responseOfUsers.ok) {
        throw new Error("Error in fetching users");
      }
      //converting response of users to json
      return responseOfUsers.json();
    })
    .then((usersData) => {
      resultObject["users"] = usersData;
      //returning todos data
      return fetch(todosUrl)
        .then((responseOfTodos) => {
          if (!responseOfTodos.ok) {
            throw new Error("Error in fetching todos");
          }

          //converting response of todos to json
          return responseOfTodos.json();
        })
        .catch((error) => {
          throw error;
        });
    })
    .then((todosData) => {
      resultObject["todos"] = todosData;
      let users = resultObject.users;
      let todos = resultObject.todos;
      //iterating through each user and todos of him and storing that data
      users.forEach((user) => {
        todos.forEach((todo) => {
          if (user.id === todo.userId) {
            if (finalObject[user.id]) {
              finalObject[user.id].push(todo);
            } else {
              finalObject[user.id] = [todo];
            }
          }
        });
      });

      return finalObject;
    })
    .catch((error) => {
      console.log("Error in fetching users and todos:", error);
    });
}

//executing the main function
fetchUsersAndTodosPerUser()
  .then((result) => {
    console.log("result=", result);
  })
  .catch((error) => {
    console.log("error in main function:", error);
  });
