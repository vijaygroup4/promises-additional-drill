//import fetch from "node-fetch";
import fetch from "node-fetch";

//function that fetches all users
function fetchAllUsers() {
  const usersUrl = "https://jsonplaceholder.typicode.com/users";

  return fetch(usersUrl)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network request not successful");
      }
      //converting that data to json
      return response.json();
    })
    .catch((error) => {
      console.log("Error fetching users:", error.message);
    });
}

//implementing the main function
fetchAllUsers()
  .then((users) => {
    console.log("Fetched Users:", users);
  })
  .catch((error) => {
    console.log("Error in main execution:", error.message);
  });
