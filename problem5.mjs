//importing fetch function
import fetch from "node-fetch";

//function to get first todo and his details
function firstTodoAndHisDetails() {
  let todosUrl = "https://jsonplaceholder.typicode.com/todos";
  let resultObject = {};
  let key = "";
  //fetching todos
  return fetch(todosUrl)
    .then((response) => {
      if (!response.ok) {
        throw new Error("network request failed");
      }
      return response.json();
    })
    .then((todos) => {
      let firstTodo = todos[0];
      let firstTodoId = firstTodo.id;
      key = firstTodoId;
      //fetching details of first todo
      return fetch(
        `https://jsonplaceholder.typicode.com/todos?userId=${firstTodoId}`
      )
        .then((detail) => {
          return detail.json();
        })
        .catch((error) => {
          throw error(error);
        });
    })
    .then((details) => {
      //storing first todo and his details in resultObject
      resultObject[key] = details;
      return resultObject;
    })
    .catch((error) => {
      console.log("error in todos function:", error);
    });
}

//execution of main function
firstTodoAndHisDetails()
  .then((result) => {
    console.log("result=", result);
  })
  .catch((error) => {
    console.log("error in main function:", error);
  });
