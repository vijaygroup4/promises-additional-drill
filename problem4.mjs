//importing fetch function
import fetch from "node-fetch";

//function to fetch users and details per user
function fetchUsersAndDetailsPerUser() {
  const usersUrl = "https://jsonplaceholder.typicode.com/users";

  let finalObject = {};
  //fetching users
  return fetch(usersUrl)
    .then((responseOfUsers) => {
      if (!responseOfUsers.ok) {
        throw new Error("Error in fetching users");
      }
      return responseOfUsers.json();
    })
    .then((userData) => {
      let users = userData;
      //recursive function to fetch details per user
      function recursive(id) {
        if (id <= users.length) {
          return fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
            .then((data) => {
              return data.json();
            })
            .then((data) => {
              finalObject[id] = {
                user: data,
              };

              return recursive(id + 1);
            })
            .catch((error) => {
              throw new Error(`Error in fetching user ${id}: ${error.message}`);
            });
        } else {
          // Resolve the Promise to terminate recursion
          return Promise.resolve();
        }
      }

      return recursive(users[0].id);
    })
    .then(() => {
      return finalObject;
    })
    .catch((error) => {
      console.log("Error in fetching users and todos:", error);
    });
}

//executing the main function
fetchUsersAndDetailsPerUser()
  .then((result) => {
    console.log("result=", result);
  })
  .catch((error) => {
    console.log("error in main function:", error);
  });
