//import fetch from "node-fetch";
import fetch from "node-fetch";

//function that fetches all todos from the API endpoint
function fetchAllTodos() {
  const todosUrl = "https://jsonplaceholder.typicode.com/todos";

  return fetch(todosUrl)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network request not successful");
      }
      //convert response to json
      return response.json();
    })
    .catch((error) => {
      console.log("Error fetching todos:", error.message);
    });
}

//implementing the main function
fetchAllTodos()
  .then((todos) => {
    console.log("Fetched Todos:", todos);
  })
  .catch((error) => {
    console.log("Error in main execution:", error.message);
  });
